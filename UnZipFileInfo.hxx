#ifndef UN_ZIP_FILE_INFO_HXX
#define UN_ZIP_FILE_INFO_HXX

#include <string>
#include <ctime>
#include <boost/filesystem/path.hpp>

class UnZipFileInfo
{
public:
  UnZipFileInfo();
  ~UnZipFileInfo();
  void setFileName(const boost::filesystem::path);
  boost::filesystem::path getFileName() const;
  void setComment(const std::string);
  std::string getComment() const;
  void setVersion(const unsigned long);
  unsigned long getVersion() const;
  void setVersionNeeded(const unsigned long);
  unsigned long getVersionNeeded() const;
  void setFlags(const unsigned long);
  unsigned long getFlags() const;
  void setCompressionMethod(const unsigned long);
  unsigned long getCompressionMethod() const;
  void setDosDate(const unsigned long);
  unsigned long getDosDate() const;
  void setCRC(const unsigned long);
  unsigned long getCRC() const;
  void setCompressedSize(const unsigned long);
  unsigned long getCompressedSize() const;
  void setUncompressedSize(const unsigned long);
  unsigned long getUncompressedSize() const;
  void setInternalAttrib(const unsigned long);
  unsigned long getInternalAttrib() const;
  void setExternalAttrib(const unsigned long);
  unsigned long getExternalAttrib() const;
  void setFolder(const bool);
  bool isFolder() const;
  void setTimeInfo(std::tm *);
  const std::tm *getTimeInfo() const;
private:
  boost::filesystem::path fileName;
  std::string comment;
  unsigned long version;
  unsigned long versionNeeded;
  unsigned long flags;
  unsigned long compressionMethod;
  unsigned long dosDate;
  unsigned long CRC;
  unsigned long compressedSize;
  unsigned long uncompressedSize;
  unsigned long internalAttrib;
  unsigned long externalAttrib;
  bool folder;
  std::tm *timeInfo;
};

#endif
