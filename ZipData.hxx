#ifndef ZIP_DATA_HXX
#define ZIP_DATA_HXX

#include <boost/filesystem/path.hpp>
#include "FileUtils.hxx"

class ZipData : public FileUtils
{
public:
  virtual bool setDestination(boost::filesystem::path) = 0;
  virtual bool setSource(boost::filesystem::path) = 0;
protected:
  boost::filesystem::path source;
  boost::filesystem::path destination;
};

#endif
