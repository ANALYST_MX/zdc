#ifndef ZIP_DATA_COMPRESSION_HXX
#define ZIP_DATA_COMPRESSION_HXX

#include <string>
#include <fstream>
#include <ios>
#include <array>
#include <boost/filesystem/path.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/date_time/posix_time/posix_time_types.hpp>
#include <minizip/zip.h>
#include "ZipData.hxx"

class ZipDataCompression : public ZipData
{
public:
  ZipDataCompression(boost::filesystem::path, int = Z_DEFAULT_COMPRESSION);
  ZipDataCompression(boost::filesystem::path, boost::filesystem::path, int = Z_DEFAULT_COMPRESSION);
  virtual ~ZipDataCompression();
  bool compress(bool = false);
protected:
  bool setDestination(boost::filesystem::path);
  bool setSource(boost::filesystem::path);
  bool addFile(zipFile, boost::filesystem::path);
  bool addFolder(zipFile, boost::filesystem::path);
  zipFile open(bool = false);
  bool close(zipFile);
  static const unsigned int BUFFER_SIZE = 8192;
private:
  boost::filesystem::path source;
  boost::filesystem::path destination;
  int compressLevel;
};

#endif
