#ifndef FILE_UTILS_HXX
#define FILE_UTILS_HXX

#include <ctime>
#include <sys/stat.h>
#include <sys/types.h>
#include <cstddef>
#include <boost/filesystem.hpp>
#include <boost/filesystem/path.hpp>

class FileUtils
{
public:
  FileUtils();
  ~FileUtils();
  bool createFolder(const boost::filesystem::path);
  bool isExists(const boost::filesystem::path);
  bool isFolder(const boost::filesystem::path);
  bool isFile(const boost::filesystem::path);
  bool isFile(const boost::filesystem::file_status);
  bool isFolder(const boost::filesystem::file_status);
  bool isFolder(const unsigned long);
  bool isFile(const unsigned long);
  bool setFileModTime(const boost::filesystem::path, std::tm *);
  unsigned int getFileAttributes(const boost::filesystem::path);
  boost::filesystem::path getPathWithoutExtension(const boost::filesystem::path);
  boost::filesystem::path getRelativePath(const boost::filesystem::path, const boost::filesystem::path);
  size_t getFileSize(const boost::filesystem::path);
  size_t getFolderSize(const boost::filesystem::path);
};

#endif
