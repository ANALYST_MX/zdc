#include "ZipDataCompression.hxx"

ZipDataCompression::ZipDataCompression(boost::filesystem::path source, int level)
  : compressLevel(level)
{
  if ( !setSource(source) )
    {
      throw std::invalid_argument("Source file not found");
    }
  setDestination(source);
}

ZipDataCompression::ZipDataCompression(boost::filesystem::path source, boost::filesystem::path destination, int level)
  : compressLevel(level)
{
  if ( !setSource(source) )
    {
      throw std::invalid_argument("Source file not found");
    }
  if ( !setDestination(destination) )
    {
      throw std::invalid_argument("Destination is not set");
    }
}

ZipDataCompression::~ZipDataCompression()
{
}

bool ZipDataCompression::compress(bool append)
{
  bool ret = false;
  zipFile zpFile = open(append);
  if ( zpFile == nullptr )
    {
      return false;
    }
  if ( isFile(source) )
    {
      addFile(zpFile, source);
      ret = true;
    }
  else if ( isFolder(source) )
    {
      addFolder(zpFile, source);
      ret = true;
    }
  close(zpFile);
  return ret;
}

zipFile ZipDataCompression::open(bool bAppend)
{
  int append = APPEND_STATUS_CREATE;
  if ( isExists(source) )
    {
      append = (bAppend ? APPEND_STATUS_ADDINZIP : APPEND_STATUS_CREATE);
    }
  return zipOpen64(destination.string().c_str(), append);
}

bool ZipDataCompression::close(zipFile zpFile)
{
  return (zipClose(zpFile, NULL) == ZIP_OK);
}

bool ZipDataCompression::addFile(zipFile zpFile, boost::filesystem::path filePath)
{
  boost::filesystem::path fileName;
  if ( isFile(source) )
    {
      fileName = getRelativePath(filePath, source.parent_path());
    }
  else if ( isFolder(source) )
    {
      fileName = getRelativePath(filePath, source);
    }

  zip_fileinfo file_info;
  file_info.internal_fa = 0;
  file_info.external_fa = getFileAttributes(filePath);

  std::time_t lastWriteTime = boost::filesystem::last_write_time(filePath);
  boost::posix_time::ptime posixTime = boost::posix_time::from_time_t(lastWriteTime);
  std::tm tm = boost::posix_time::to_tm(posixTime);
  file_info.dosDate = 0;
  file_info.tmz_date.tm_year = tm.tm_year;
  file_info.tmz_date.tm_mon  = tm.tm_mon;
  file_info.tmz_date.tm_mday = tm.tm_mday;
  file_info.tmz_date.tm_hour = tm.tm_hour;
  file_info.tmz_date.tm_min  = tm.tm_min;
  file_info.tmz_date.tm_sec  = tm.tm_sec;

  std::ifstream inputFile;
  inputFile.open(filePath.string().c_str(), std::ios::in | std::ios::binary);
  if ( !inputFile.is_open() )
    {
      return false;
    }

  int ret = zipOpenNewFileInZip64(zpFile, fileName.string().c_str(), &file_info, NULL, 0, NULL, 0, NULL, (compressLevel == 0 ? Z_DEFLATED : Z_NO_COMPRESSION), compressLevel, getFileSize(filePath));

  if ( ret == ZIP_OK )
    {
      std::array<char, BUFFER_SIZE> buffer;
      unsigned long bytesRead = 0;
      unsigned long fileSize = 0;

      while ( ret == ZIP_OK && inputFile.good() )
	{
	  inputFile.read(buffer.data(), buffer.size());
	  bytesRead = inputFile.gcount();
	  fileSize += bytesRead;

	  if ( bytesRead )
	    {
	      ret = zipWriteInFileInZip(zpFile, buffer.data(), bytesRead);
	    }
	  else
	    {
	      break;
	    }
	}
    }

  inputFile.close();
  zipCloseFileInZip(zpFile);
  
  return (ret == ZIP_OK);
}

bool ZipDataCompression::addFolder(zipFile zpFile, boost::filesystem::path folderPath)
{
  zip_fileinfo folder_info;
  folder_info.internal_fa = 0;
  folder_info.external_fa = getFileAttributes(folderPath);

  std::time_t lastWriteTime = boost::filesystem::last_write_time(folderPath);
  boost::posix_time::ptime posixTime = boost::posix_time::from_time_t(lastWriteTime);
  std::tm tm = boost::posix_time::to_tm(posixTime);
  folder_info.dosDate = 0;
  folder_info.tmz_date.tm_year = tm.tm_year;
  folder_info.tmz_date.tm_mon  = tm.tm_mon;
  folder_info.tmz_date.tm_mday = tm.tm_mday;
  folder_info.tmz_date.tm_hour = tm.tm_hour;
  folder_info.tmz_date.tm_min  = tm.tm_min;
  folder_info.tmz_date.tm_sec  = tm.tm_sec;

  boost::filesystem::path folderName;
  folderName = getRelativePath(folderPath, source);
  
  if ( !folderName.string().empty() )
    {
      zipOpenNewFileInZip64(zpFile, (folderName.string()+"/").c_str(), &folder_info, NULL, 0, NULL, 0, NULL,(compressLevel == 0 ? Z_DEFLATED : Z_NO_COMPRESSION), compressLevel, getFolderSize(folderPath));
      zipCloseFileInZip(zpFile);
    }

  auto end = boost::filesystem::recursive_directory_iterator();
  for( auto iter = boost::filesystem::recursive_directory_iterator(folderPath) ; iter != end ; ++iter )
    {
      if ( isFile(iter->status()) )
	{
	  addFile(zpFile, iter->path());
	}
      else if ( isFolder(iter->status()) )
	{
	  addFolder(zpFile, iter->path());
	}
    }
  
  return true;
}

bool ZipDataCompression::setDestination(boost::filesystem::path newDestination)
{
  destination = newDestination.parent_path() / (newDestination.stem().string() + ".zip");
  bool res = createFolder(destination.parent_path());
  
  return res;
}

bool ZipDataCompression::setSource(boost::filesystem::path newSource)
{
  bool res = isExists(newSource);
  if ( res )
    {
      source = newSource;
    }

  return res;
}

