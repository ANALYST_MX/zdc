GXX := g++
GXXFLAG := -std=c++11 -Wall -g
MAIN := main
CXX := .cxx
HXX := .hxx
OBJ := .o
CURRENT_DIR := $(shell pwd)
OBJ_LIST := FileUtils$(OBJ) ZipData$(OBJ) ZipDataUncompression$(OBJ) ZipDataCompression$(OBJ) UnZipFileInfo$(OBJ)
LIB_LIST := -lminizip -lboost_system -lboost_filesystem -lboost_date_time -lboost_program_options
all: $(MAIN)
$(MAIN): $(MAIN)$(OBJ) $(OBJ_LIST)
	$(GXX) $(GXXFLAG) $(MAIN)$(OBJ) $(OBJ_LIST) -o $(MAIN) $(LIB_LIST)
$(MAIN)$(OBJ): $(MAIN)$(CXX) $(MAIN)$(HXX)
	$(GXX) $(GXXFLAG) -c $(MAIN)$(CXX) -o $(MAIN)$(OBJ)
ZipDataCompression$(OBJ): ZipDataCompression$(CXX) ZipDataCompression$(HXX) ZipData$(OBJ)
	$(GXX) $(GXXFLAG) -c ZipDataCompression$(CXX) -o ZipDataCompression$(OBJ)
ZipDataUncompression$(OBJ): ZipDataUncompression$(CXX) ZipDataUncompression$(HXX) ZipData$(OBJ) UnZipFileInfo$(OBJ)
	$(GXX) $(GXXFLAG) -c ZipDataUncompression$(CXX) -o ZipDataUncompression$(OBJ)
ZipData$(OBJ): ZipData$(CXX) ZipData$(HXX) FileUtils$(OBJ)
	$(GXX) $(GXXFLAG) -c ZipData$(CXX) -o ZipData$(OBJ)
FileUtils$(OBJ): FileUtils$(CXX) FileUtils$(HXX)
	$(GXX) $(GXXFLAG) -c FileUtils$(CXX) -o FileUtils$(OBJ) $(LIB_LIST)
UnZipFileInfo$(OBJ): UnZipFileInfo$(CXX) UnZipFileInfo$(HXX)
	$(GXX) $(GXXFLAG) -c UnZipFileInfo$(CXX) -o UnZipFileInfo$(OBJ)
.phony: clean
clean:
	rm -rf $(MAIN) *$(OBJ) *.out *~
