#ifndef ZIP_DATA_UNCOMPRESSION_HXX
#define ZIP_DATA_UNCOMPRESSION_HXX

#include <iostream>
#include <string>
#include <cstddef>
#include <fstream>
#include <memory>
#include <array>
#include <boost/filesystem/path.hpp>
#include <minizip/unzip.h>
#include "ZipData.hxx"
#include "UnZipFileInfo.hxx"

class ZipDataUncompression : public ZipData
{
public:
  ZipDataUncompression(boost::filesystem::path);
  ZipDataUncompression(boost::filesystem::path, boost::filesystem::path);
  virtual ~ZipDataUncompression();
  bool uncompress();
protected:
  bool setDestination(boost::filesystem::path);
  bool setSource(boost::filesystem::path);
  ZPOS64_T getFileCount(const unzFile);
  bool gotoFirstFile(unzFile uzFile);
  bool gotoNextFile(unzFile uzFile);
  bool uncompressFile(const unzFile);
  UnZipFileInfo *getFileInfo(const unzFile uzFile);
  const unsigned int BUFFER_SIZE = 8192;
private:
  boost::filesystem::path source;
  boost::filesystem::path destination;
};

#endif
