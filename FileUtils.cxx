#include "FileUtils.hxx"

FileUtils::FileUtils()
{
}

FileUtils::~FileUtils()
{
}

bool FileUtils::createFolder(const boost::filesystem::path folder)
{
  bool res = false;
  
  if ( !isExists(folder) )
    {
      res = boost::filesystem::create_directories(folder);
    }
  else
    {
      if ( isFolder(folder) )
	{
	  res = true;
	}
    }

  return res; 
}

bool FileUtils::isExists(const boost::filesystem::path folder)
{
  return boost::filesystem::exists(folder);
}

bool FileUtils::isFolder(const boost::filesystem::path folder)
{
  return boost::filesystem::is_directory(folder);
}

bool FileUtils::isFile(const boost::filesystem::file_status file)
{
  return boost::filesystem::is_regular_file(file);
}

bool FileUtils::isFolder(const boost::filesystem::file_status folder)
{
  return boost::filesystem::is_directory(folder);
}

bool FileUtils::isFile(const boost::filesystem::path file)
{
  return boost::filesystem::is_regular_file(file);
}

bool FileUtils::isFolder(const unsigned long mode)
{
#ifndef S_IFMT
#ifdef _S_IFMT
#define S_IFMT _S_IFMT
#else
#error S_IFMT not defined
#endif
#endif
#if defined S_IFDIR && defined S_IFMT
  return (((mode) & S_IFMT) == S_IFDIR);
#else
#error S_IFDIR not defined
#endif
}

bool FileUtils::isFile(const unsigned long mode)
{
  #ifndef S_IFMT
  #ifdef _S_IFMT
  #define S_IFMT _S_IFMT
  #else
  #error S_IFMT not defined
  #endif
  #endif
  #if defined S_IFREG && defined S_IFMT
  return (((mode) & S_IFMT) == S_IFREG);
  #else
  #error S_IFREG not defined
  #endif
}


bool FileUtils::setFileModTime(const boost::filesystem::path filePath, std::tm *timeInfo)
{
  bool res = false;
  std::time_t lastWriteTime = std::mktime(timeInfo);
  if( lastWriteTime >= 0 )
    {
      boost::filesystem::last_write_time(filePath, lastWriteTime);
      res = true;
    }
  
  return res;
}

unsigned int FileUtils::getFileAttributes(const boost::filesystem::path filePath)
{
  unsigned int attrib = 0;
  struct stat st;
  if ( stat(filePath.string().c_str(), &st) == 0 )
    {
      attrib = st.st_mode;
    }
  
  return attrib;
}

boost::filesystem::path FileUtils::getPathWithoutExtension(const boost::filesystem::path filePath)
{
  return boost::filesystem::change_extension(filePath, "");
}

boost::filesystem::path FileUtils::getRelativePath(const boost::filesystem::path path, const boost::filesystem::path base)
{
  if ( path.has_root_path() )
    {
      if ( path.root_path() != base.root_path() )
	{
	  return path;
	}
      else
	{
	  return getRelativePath(path.relative_path(), base.relative_path());
	}
    }
  else
    {
      if ( base.has_root_path() )
	{
	  throw "cannot uncomplete a path relative path from a rooted base";
	}
      else
	{
	  auto path_it = path.begin();
	  auto base_it = base.begin();
	  for ( ; path_it != path.end() && base_it != base.end(); ++path_it, ++base_it )
	    {
	      if ( *path_it != *base_it )
		{
		  break;
		}
	    }
	  boost::filesystem::path result;
	  for ( ; base_it != base.end(); ++base_it )
	    {
	      result /= "..";
	    }
	  for ( ; path_it != path.end(); ++path_it )
	    {
	      result /= *path_it;
	    }
	  return result;
	}
    }
}

size_t FileUtils::getFileSize(const boost::filesystem::path file)
{
  if ( isExists(file) && isFile(file) )
    {
      return boost::filesystem::file_size(file);
    }
  return -1;
}

size_t FileUtils::getFolderSize(const boost::filesystem::path folder)
{
  if( isExists(folder) )
    {
      size_t size;
      auto end = boost::filesystem::recursive_directory_iterator();
      for( auto iter = boost::filesystem::recursive_directory_iterator(folder) ; iter != end ; ++iter )
	{
	  if ( isFile(iter->path()) )
	    {
	      size += boost::filesystem::file_size(iter->path());
	    }
	}
      return size;
  }
  return -1;
}
