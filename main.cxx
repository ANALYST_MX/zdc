#include "main.hxx"

int main(int argc, char *argv[])
{
  po::options_description desc("Options");
  desc.add_options()
    ("help,h", "produce help message")
    ("source,s", po::value< std::string >()->required(), "Source")
    ("destination,d", po::value< std::string >(), "Destination")
    ("unzip,u", "Unzip")
    ("zip,z", "Zip")
    ;

  po::variables_map vm;
  po::store(po::parse_command_line(argc, argv, desc), vm);

  if ( vm.count("help")
       || (!vm.count("unzip") && !vm.count("zip"))
       || (vm.count("unzip") && vm.count("zip")) )
    {
      std::cout << desc << std::endl;
      return EXIT_SUCCESS;
    }

  try
    {
      po::notify(vm);
      if ( vm.count("unzip") )
	{
	  if ( vm.count("source") )
	    {
	      if ( vm.count("destination") )
		{
		  ZipDataUncompression zdu(vm["source"].as<std::string>(), vm["destination"].as<std::string>());
		  zdu.uncompress();
		}
	      else
		{
		  ZipDataUncompression zdu(vm["source"].as<std::string>());
		  zdu.uncompress();
		}
	    }
	  else
	    {
	      std::cout << desc << std::endl;
	      return EXIT_FAILURE;
	    }
	}
      if ( vm.count("zip") )
	{
	  if ( vm.count("source") )
	    {
	      if ( vm.count("destination") )
		{
		  ZipDataCompression zdc(vm["source"].as<std::string>(), vm["destination"].as<std::string>());
		  zdc.compress();
		}
	      else
		{
		  ZipDataCompression zdc(vm["source"].as<std::string>());
		  zdc.compress();
		}
	    }
	  else
	    {
	      std::cout << desc << std::endl;
	      return EXIT_FAILURE;
	    }
	}
    }
  catch (po::error &e)
    {
      std::cerr << e.what() << std::endl;
      return EXIT_FAILURE;
    }

  return EXIT_SUCCESS;
}
