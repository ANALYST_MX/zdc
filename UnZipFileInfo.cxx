#include "UnZipFileInfo.hxx"

UnZipFileInfo::UnZipFileInfo()
{
}

UnZipFileInfo::~UnZipFileInfo()
{
  delete timeInfo;
}

void UnZipFileInfo::setFileName(const boost::filesystem::path newFileName)
{
  fileName = newFileName;
}

boost::filesystem::path UnZipFileInfo::getFileName() const
{
  return fileName;
}

void UnZipFileInfo::setComment(const std::string newComment)
{
  comment = newComment;
}

std::string UnZipFileInfo::getComment() const
{
  return comment;
}

void UnZipFileInfo::setVersion(const unsigned long newVersion)
{
  version = newVersion;
}

unsigned long UnZipFileInfo::getVersion() const
{
  return version;
}

void UnZipFileInfo::setVersionNeeded(const unsigned long newVersionNeeded)
{
  versionNeeded = newVersionNeeded;
}

unsigned long UnZipFileInfo::getVersionNeeded() const
{
  return versionNeeded;
}

void UnZipFileInfo::setFlags(const unsigned long newFlags)
{
  flags = newFlags;
}

unsigned long UnZipFileInfo::getFlags() const
{
  return flags;
}

void UnZipFileInfo::setCompressionMethod(const unsigned long newCompressionMethod)
{
  compressionMethod = newCompressionMethod;
}

unsigned long UnZipFileInfo::getCompressionMethod() const
{
  return compressionMethod;
}

void UnZipFileInfo::setDosDate(const unsigned long newDosDate)
{
  dosDate = newDosDate;
}

unsigned long UnZipFileInfo::getDosDate() const
{
  return dosDate;
}

void UnZipFileInfo::setCRC(const unsigned long newCRC)
{
  CRC = newCRC;
}

unsigned long UnZipFileInfo::getCRC() const
{
  return CRC;
}

void UnZipFileInfo::setCompressedSize(const unsigned long newCompressedSize)
{
  compressedSize = newCompressedSize;
}

unsigned long UnZipFileInfo::getCompressedSize() const
{
  return compressedSize;
}

void UnZipFileInfo::setUncompressedSize(const unsigned long newUncompressedSize)
{
  uncompressedSize = newUncompressedSize;
}

unsigned long UnZipFileInfo::getUncompressedSize() const
{
  return uncompressedSize;
}

void UnZipFileInfo::setInternalAttrib(const unsigned long newInternalAttrib)
{
  internalAttrib = newInternalAttrib;
}

unsigned long UnZipFileInfo::getInternalAttrib() const
{
  return internalAttrib;
}

void UnZipFileInfo::setExternalAttrib(const unsigned long newExternalAttrib)
{
  externalAttrib = newExternalAttrib;
}

unsigned long UnZipFileInfo::getExternalAttrib() const
{
  return externalAttrib;
}

void UnZipFileInfo::setFolder(const bool newIsFolder)
{
  folder = newIsFolder;
}

bool UnZipFileInfo::isFolder() const
{
  return folder;
}

void UnZipFileInfo::setTimeInfo(std::tm * newTimeInfo)
{
  timeInfo = newTimeInfo;
}

const std::tm *UnZipFileInfo::getTimeInfo() const
{
  return timeInfo;
}
