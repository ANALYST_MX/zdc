#include "ZipDataUncompression.hxx"

ZipDataUncompression::ZipDataUncompression(boost::filesystem::path source)
{
  if ( !setSource(source) )
    {
      throw std::invalid_argument("Source file not found");
    }
  setDestination(getPathWithoutExtension(source));
}

ZipDataUncompression::ZipDataUncompression(boost::filesystem::path source, boost::filesystem::path destination)
{
  if ( !setSource(source) )
    {
      throw std::invalid_argument("Source file not found");
    }
  if ( !setDestination(destination) )
    {
      throw std::invalid_argument("Destination is not set");
    }
}

ZipDataUncompression::~ZipDataUncompression()
{
  source = "";
  destination = "";
}

bool ZipDataUncompression::uncompress()
{
  unzFile uzFile = unzOpen64(source.string().c_str());
  if ( uzFile == nullptr )
    {
      return false;
    }
  if ( getFileCount(uzFile) == 0 )
    {
      return false;
    }
  if ( !gotoFirstFile(uzFile) )
    {
      return false;
    }
  do
    {
      if ( !uncompressFile(uzFile) )
	{
	  return false;
	}
    }
  while ( gotoNextFile(uzFile) );
  
  return true;
}

bool ZipDataUncompression::uncompressFile(const unzFile uzFile)
{
  bool res = false;
  std::shared_ptr<UnZipFileInfo> info(getFileInfo(uzFile));
  if ( info == nullptr )
    {
      return false;
    }

  if ( info->isFolder() )
    {
      boost::filesystem::path folderPath = destination / info->getFileName();
      res = createFolder(folderPath);
      if( res )
	{
	  res = setFileModTime(folderPath, const_cast<std::tm *>(info->getTimeInfo()));
	}
      return res;
    }

  boost::filesystem::path filePath = destination / info->getFileName();
  res = createFolder(filePath.parent_path());
  if ( !res )
    {
      return false;
    }

  std::ofstream outputFile;
  outputFile.open(filePath.string().c_str(), std::ios::out | std::ios::binary);
  if ( !outputFile.is_open() )
    {
      return false;
    }
  if ( unzOpenCurrentFile(uzFile) != UNZ_OK )
    {
      return false;
    }

  int ret = UNZ_OK;
  char buffer[BUFFER_SIZE];

  do
    {
      ret = unzReadCurrentFile(uzFile, buffer, BUFFER_SIZE);
      if ( ret > 0 )
	{
	  outputFile.write(buffer, ret);
	}
    }
  while ( ret > 0 );

  outputFile.close();
  unzCloseCurrentFile(uzFile);

  if ( ret == UNZ_OK )
    {
      setFileModTime(filePath, const_cast<std::tm *>(info->getTimeInfo()));
      res = true;
    }
  else
    {
      res = false;
    }
  
  return res;
}

ZPOS64_T ZipDataUncompression::getFileCount(const unzFile uzFile)
{
  ZPOS64_T nbFile = 0;
  unz_global_info64 info;
  if ( unzGetGlobalInfo64(uzFile, &info) == UNZ_OK )
    {
      nbFile = info.number_entry;
    }

  return nbFile;
}

UnZipFileInfo *ZipDataUncompression::getFileInfo(const unzFile uzFile)
{
  UnZipFileInfo *info = nullptr;
  unz_file_info64 file_info;
  memset(&file_info, 0, sizeof(file_info));

  const unsigned int COMMENT_SIZE = 256;
  const unsigned int MAX_PATH = 256;
  std::array<char, MAX_PATH> fileName;
  std::array<char, COMMENT_SIZE> comment;
  if ( unzGetCurrentFileInfo64(uzFile, &file_info, fileName.data(), fileName.size(), NULL, 0, comment.data(), comment.size()) == UNZ_OK )
    {
      info = new UnZipFileInfo;
      info->setVersion(file_info.version);
      info->setVersionNeeded(file_info.version_needed);
      info->setFlags(file_info.flag);
      info->setCompressionMethod(file_info.compression_method);
      info->setDosDate(file_info.dosDate);
      info->setCRC(file_info.crc);
      info->setCompressedSize(file_info.compressed_size);
      info->setUncompressedSize(file_info.uncompressed_size);
      info->setInternalAttrib(file_info.internal_fa);
      info->setExternalAttrib(file_info.external_fa);
      info->setComment(std::string(comment.data()));
      info->setFileName(boost::filesystem::path(fileName.data()));
      std::tm *timeInfo = static_cast<std::tm *>(malloc(sizeof(*timeInfo)));
      timeInfo->tm_year = file_info.tmu_date.tm_year - 1900;
      timeInfo->tm_mon  = file_info.tmu_date.tm_mon;
      timeInfo->tm_mday = file_info.tmu_date.tm_mday;
      timeInfo->tm_hour = file_info.tmu_date.tm_hour - 1;
      timeInfo->tm_min  = file_info.tmu_date.tm_min;
      timeInfo->tm_sec  = file_info.tmu_date.tm_sec;
      info->setTimeInfo(timeInfo);
      info->setFolder(isFolder(file_info.external_fa));
    }
  return info;
}


bool ZipDataUncompression::gotoFirstFile(unzFile uzFile)
{
  return ( unzGoToFirstFile(uzFile) == UNZ_OK );
}

bool ZipDataUncompression::gotoNextFile(unzFile uzFile)
{
  return ( unzGoToNextFile(uzFile) == UNZ_OK );
}

bool ZipDataUncompression::setSource(boost::filesystem::path newSource)
{
  bool res = isExists(newSource);
  if ( res )
    {
      source = newSource;
    }

  return res;
}

bool ZipDataUncompression::setDestination(boost::filesystem::path newDestination)
{
  bool res = createFolder(newDestination);
  destination = newDestination;
  
  return res;
}
